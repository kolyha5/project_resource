-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 13 2016 г., 22:56
-- Версия сервера: 5.5.50
-- Версия PHP: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `my_project`
--

-- --------------------------------------------------------

--
-- Структура таблицы `dolzhnosts`
--

CREATE TABLE IF NOT EXISTS `dolzhnosts` (
  `id_dolzhnost` int(8) NOT NULL,
  `name_dolzhnost` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dolzhnosts`
--

INSERT INTO `dolzhnosts` (`id_dolzhnost`, `name_dolzhnost`) VALUES
(1, 'Руководитель организации'),
(2, 'Менеджер');

-- --------------------------------------------------------

--
-- Структура таблицы `personals`
--

CREATE TABLE IF NOT EXISTS `personals` (
  `id_personal` int(8) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `name_personal` varchar(255) NOT NULL,
  `id_dolzhnost` int(8) NOT NULL,
  `date_rozhd` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `last_enter_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `personals`
--

INSERT INTO `personals` (`id_personal`, `firstname`, `name_personal`, `id_dolzhnost`, `date_rozhd`, `email`, `tel`, `last_enter_date`) VALUES
(1, 'Петров', 'Петр', 1, '2016-11-01', 'petr@petr.ru', '+79001234567', '2016-11-19'),
(2, 'Иванов', 'Иван', 2, '2016-11-17', 'ivanov@ivan.ru', '+79004567890', '2016-11-02'),
(3, 'Сидоров', 'Сидор', 1, '0000-00-00', '', '', '0000-00-00'),
(4, 'Федоров', 'Федор', 1, '0000-00-00', '', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Структура таблицы `preoritets_tasks`
--

CREATE TABLE IF NOT EXISTS `preoritets_tasks` (
  `id_preoritet_task` int(8) NOT NULL,
  `name_preoritet_task` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `preoritets_tasks`
--

INSERT INTO `preoritets_tasks` (`id_preoritet_task`, `name_preoritet_task`) VALUES
(1, 'Средний'),
(2, 'Высокий'),
(3, 'Низкий');

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id_project` int(8) NOT NULL,
  `name_project` varchar(255) NOT NULL,
  `id_manager` int(8) NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `id_status_project` int(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id_project`, `name_project`, `id_manager`, `start_date`, `finish_date`, `id_status_project`) VALUES
(1, 'Проект №1', 1, '2016-11-20', '0000-00-00', 1),
(2, 'Проект №2', 1, '2016-11-08', '0000-00-00', 1),
(3, 'Проект №3', 1, '2016-09-07', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `status_project`
--

CREATE TABLE IF NOT EXISTS `status_project` (
  `id_status_project` int(8) NOT NULL,
  `name_status_project` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `status_project`
--

INSERT INTO `status_project` (`id_status_project`, `name_status_project`) VALUES
(1, 'Выполняется'),
(2, 'Закрыт');

-- --------------------------------------------------------

--
-- Структура таблицы `status_task`
--

CREATE TABLE IF NOT EXISTS `status_task` (
  `id_status_task` int(8) NOT NULL,
  `name_status_task` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `status_task`
--

INSERT INTO `status_task` (`id_status_task`, `name_status_task`) VALUES
(1, 'Ожидает выполнения'),
(2, 'Выполняется'),
(3, 'Ожидает проверки'),
(4, 'Закрыта'),
(5, 'Пауза');

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id_task` int(8) NOT NULL,
  `name_task` varchar(255) NOT NULL,
  `description_task` text NOT NULL,
  `id_status_task` int(8) NOT NULL,
  `id_preoritet_task` int(8) NOT NULL,
  `deadline_task` varchar(255) NOT NULL,
  `id_ispolnitel` int(8) NOT NULL,
  `id_postanov` int(8) NOT NULL,
  `id_project` int(8) NOT NULL,
  `date_postanovka` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `finish_date` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id_task`, `name_task`, `description_task`, `id_status_task`, `id_preoritet_task`, `deadline_task`, `id_ispolnitel`, `id_postanov`, `id_project`, `date_postanovka`, `start_date`, `finish_date`) VALUES
(1, 'Задача №1', 'Описание задачи 1', 3, 1, '30.12.2016', 1, 2, 1, '2016-11-20', '31.12.2016', '31.12.2016'),
(2, 'Задача 2', 'Описаие задачи 2', 2, 2, '01.12.2016', 2, 2, 2, '2016-11-19', '11.11.2011', '12.12.2016'),
(20, 'Задача №6', 'Описание 6ой задачи', 5, 1, '11.12.2016', 2, 2, 1, '04.12.2016', '31.12.2016', '0'),
(22, 'Задача №11', 'Краткое описание для задачи №11', 2, 1, '01.12.2016', 2, 1, 2, '12.12.2016', '0', '0'),
(23, 'Задача №12', 'Описание для задачи  №12', 1, 1, '20.12.2016', 3, 2, 2, '12.12.2016', '0', '0'),
(24, 'Задача №13', 'Описание задачи 13', 2, 1, '01.12.2016', 3, 4, 3, '12.12.2016', '31.12.2016', '31.12.2016'),
(25, 'Задача №14', 'Описание для задачи №14', 4, 1, '30.12.2016', 1, 4, 1, '12.12.2016', '0', '31.12.2016'),
(26, 'Задача №15', 'Описание задачи №15', 1, 1, '16.12.2016', 4, 2, 2, '12.12.2016', '0', '0'),
(27, 'Задача №16', 'Описание задачи №16', 1, 1, '28.12.2016', 1, 3, 2, '12.12.2016', '0', '0'),
(28, 'Задача №16', 'Описание задачи №16', 1, 1, '20.12.2016', 2, 1, 1, '13.12.2016', '0', '0');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `dolzhnosts`
--
ALTER TABLE `dolzhnosts`
  ADD PRIMARY KEY (`id_dolzhnost`);

--
-- Индексы таблицы `personals`
--
ALTER TABLE `personals`
  ADD PRIMARY KEY (`id_personal`);

--
-- Индексы таблицы `preoritets_tasks`
--
ALTER TABLE `preoritets_tasks`
  ADD PRIMARY KEY (`id_preoritet_task`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id_project`);

--
-- Индексы таблицы `status_project`
--
ALTER TABLE `status_project`
  ADD PRIMARY KEY (`id_status_project`);

--
-- Индексы таблицы `status_task`
--
ALTER TABLE `status_task`
  ADD PRIMARY KEY (`id_status_task`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id_task`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `dolzhnosts`
--
ALTER TABLE `dolzhnosts`
  MODIFY `id_dolzhnost` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `personals`
--
ALTER TABLE `personals`
  MODIFY `id_personal` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `preoritets_tasks`
--
ALTER TABLE `preoritets_tasks`
  MODIFY `id_preoritet_task` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id_project` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `status_project`
--
ALTER TABLE `status_project`
  MODIFY `id_status_project` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `status_task`
--
ALTER TABLE `status_task`
  MODIFY `id_status_task` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id_task` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
