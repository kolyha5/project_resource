<?php
class PersonalController extends AbstractController{
	protected $title;
	protected $data_personal;

	public function __construct(){
		parent::__construct(new View(DIR_TPL));
	}

	public function actionIndex(){
		$this->title = "Список сотрудников";
		$this->data_personal = $this->selectAllPersonal();
		$this->render('personal');
	}

	public function actionView_personal(){
		$this->title = "Просмотр профиля сотрудника";
		$this->render('view_personal');
	}

	public function actionEdit_personal(){
		$this->title = "Редактирование профиля сотрудника";
		$this->render('edit_personal');
	}

	public function actionNew_personal(){
		$this->title = "Создание нового сотрудника";
		$this->render('edit_personal');
	}

	protected function render($file){
		$params = array(
			'title' => $this->title,
			'data_personal' => $this->data_personal
		);

		$this->view->render($file, $params);
	}

	private function selectAllPersonal(){
		$mysql = $this->mysql_connect();

		$sql = "SELECT firstname, name_personal, name_dolzhnost
				FROM personals
				INNER JOIN dolzhnosts ON dolzhnosts.id_dolzhnost = personals.id_dolzhnost";
		$result = $mysql->query($sql);
		$data = $result->fetch_all(MYSQLI_ASSOC);
		//echo "<pre>";
		//print_r($data);
		//exit();
		return $data;
	}
}