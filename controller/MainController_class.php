<?php
/**
 * контроллер для главной страницы
 */
class MainController extends AbstractController{
	protected $title;
    protected $task_success;
    protected $task_all;
    protected $task_warning;

	public function __construct(){
		parent::__construct(new View(DIR_TPL));
	}

	public function actionIndex(){
		$this->title = "Главная";
		$this->task_success = $this->taskSuccess();
        $this->task_all = $this->taskAll();
        $this->task_warning = $this->taskWarning();

		$this->render('index');
	}

    //потом удалить
	private function settingsPage(){
		$mysql = $this->mysql_connect();
		$sql = "select * from settings";
		$result = $mysql->query($sql);
		$data = $result->fetch_assoc();
		return $data;
	}



	protected function render($file){
		$params = array();
		$params['title'] = $this->title;
	    $params['task_success'] = $this->task_success;
	    $params['task_all'] = $this->task_all;
	    $params['task_warning'] = $this->task_warning;

		$this->view->render($file, $params);
	}

	private function taskSuccess(){
	    $mysql = $this->mysql_connect();
        $sql = "SELECT COUNT(*) as kol
                FROM tasks
                WHERE finish_date != 0
        ";
        $result = $mysql->query($sql);
        $data = $result->fetch_all(MYSQL_ASSOC);
        //$this->debug($data[0]);
        return $data[0];
    }

    private function taskAll(){
        $mysql = $this->mysql_connect();
        $sql = "SELECT COUNT(*) AS kol
                FROM tasks
        ";
        $result = $mysql->query($sql);
        $data = $result->fetch_all(MYSQL_ASSOC);
        //$this->debug($data[0]);
        return $data[0];
    }

    private function taskWarning(){
        $date = date("d.m.Y");
        $mysql = $this->mysql_connect();
        $sql = "SELECT COUNT(*) AS kol
                FROM tasks
                where deadline_task < '$date' AND finish_date = 0
        ";
        $result = $mysql->query($sql);
        $data = $result->fetch_all(MYSQL_ASSOC);
        //$this->debug($data[0]);
        return $data[0];
    }
}