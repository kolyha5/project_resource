<?php
/**
 * контроллер страницы Проекты
 */
class ProjectController extends AbstractController{
	protected $title;
	protected $data_project;

	public function __construct(){
		parent::__construct(new VIEW(DIR_TPL));
	}

	public function actionIndex(){
		$this->title = "Проекты";
		$this->data_project = $this->selectAllProject();
		$this->render('project');
	}

	public function render($file){
		$params = array(
			'title'=> $this->title,
			'data_project'=> $this->data_project
		);


		$this->view->render($file, $params);
	}

	private function selectAllProject(){
		$mysql = $this->mysql_connect();
		$sql = "SELECT name_project, start_date, firstname, name_status_project
				FROM projects
				INNER JOIN personals ON projects.id_manager=personals.id_personal
				INNER JOIN status_project ON projects.id_status_project=status_project.id_status_project";
		$result = $mysql->query($sql);
		$data = $result->fetch_all(MYSQLI_ASSOC);
		//$this->debug($data);
		return $data;
	}
}