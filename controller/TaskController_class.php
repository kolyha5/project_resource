<?php
/**
 * контроллер страницы Задачи
 */
class TaskController extends AbstractController{
	protected $title;
	protected $data_task;
    protected $personal;
    protected $project;
    protected $getTask;
    protected $prioritety;

	public function __construct(){
		parent::__construct(new View(DIR_TPL));
	}

	public function actionIndex(){
		$this->title = "Список задач";
		$this->data_task = $this->selectAllTask();
		$this->render('task');
	}

	public function actionView_task(){
		$this->title = "Просмотр задачи";
        $this->getTask = $this->getTask($_GET['id_task']);
		$this->render('view_task');
	}

	public function actionNew_task(){
		$this->title = "Создание новой задачи";
        $this->personal = $this->selectAllPersonals();
        $this->project = $this->selectAllProject();
        $this->prioritety = $this->selectAllPrioritety();
		$this->render('edit_task');
	}

	protected function render($file){
		$params = array(
			'title' => $this->title,
			'data_task' => $this->data_task,
            'personal' => $this->personal,
            'project' => $this->project,
            'getTask' => $this->getTask,
            'prioritety' => $this->prioritety
		);

		$this->view->render($file, $params);
	}

	private function selectAllTask(){
		$mysql = $this->mysql_connect();
		$sql = "SELECT id_task, name_task, deadline_task, name_status_task, name_preoritet_task, firstname AS postanov, (SELECT COUNT(*) FROM tasks WHERE id_preoritet_task=1) AS kol
				FROM tasks
				INNER JOIN status_task ON status_task.id_status_task=tasks.id_status_task
				INNER JOIN preoritets_tasks ON preoritets_tasks.id_preoritet_task=tasks.id_preoritet_task
				INNER JOIN personals ON personals.id_personal=tasks.id_postanov
		";
		$result = $mysql->query($sql);
		$data = $result->fetch_all(MYSQLI_ASSOC);
		//$this->debug($data);
        return $data;
	}

	public function getTask($id_task){
	    $mysql = $this->mysql_connect();
        $sql = "SELECT *
                FROM tasks
                INNER JOIN projects ON projects.id_project = tasks.id_project
                INNER JOIN personals ON personals.id_personal = id_ispolnitel
                WHERE id_task = '$id_task'
        ";
        $result = $mysql->query($sql);
        $data = $result->fetch_all(MYSQLI_ASSOC);
        //$this->debug($data);
        return $data[0];
    }

	private function selectAllPersonals(){
	    $mysql = $this->mysql_connect();
        $sql = "SELECT id_personal, name_personal, firstname
                FROM personals
        ";
        $result = $mysql->query($sql);
        $data = $result->fetch_all(MYSQLI_ASSOC);
        return $data;
    }

    private function selectAllProject(){
        $mysql = $this->mysql_connect();
        $sql = "SELECT id_project, name_project
                FROM projects
        ";
        $result = $mysql->query($sql);
        $data = $result->fetch_all(MYSQLI_ASSOC);
        return $data;
    }

    private function selectAllPrioritety(){
        $mysql = $this->mysql_connect();
        $sql = "SELECT *
                FROM preoritets_tasks
        ";
        $result = $mysql->query($sql);
        $data = $result->fetch_all(MYSQL_ASSOC);
        //$this->debug($data);
        return $data;
    }
}