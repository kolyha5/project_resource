<?
if(isset($_POST['btn_save'])){
	header("Location: /?page=task");
	exit();
}
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?=$title;?></title>
	<link rel="stylesheet" href="/tpl/css/bootstrap.min.css">
	<link rel="stylesheet" href="/tpl/css/normalize.css">
	<!--my css-->
	<link rel="stylesheet" href="/tpl/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--navigation-->
					<nav class="navbar navbar-default" role="navigation">
						<div class="container">
							<div class="navbar-header">
								<a href="/" class="navbar-brand">PMS</a>
								<p class="navbar-text">Система управления проектами</p>
							</div>
							<ul class="nav navbar-nav">
								<?php include "/tpl/inc/menu.php"; ?>
							</ul>
							<button type="button" id="enter_site" class="btn btn-default navbar-btn">Войти</button>
						</div>
					</nav>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--тут будут выводиться сообщения-->
				</div>
			</div>
		</div>

		<!--content-->
		<div class="container">
			<div class="row">
				<!--left-bar-->
				<div class="col-md-2">
					<div class="container">
						<div class="col-md-2">
							<div class="col-md-10">
								<img src="/tpl/images/avatar.png" alt="" class="img-responsive img-thumbnail">
							</div>
							<div class="name-user">
								<h4>Николай Ляхов</h4>
							</div>
						</div>
					</div>
					<ul class="nav nav-pils nav-stacked">
						<?php include "/tpl/inc/left_menu.php";?>
					</ul>
				</div>
				
				<!--right-bar-->
				<div class="col-md-10">
					<div class="page-header">
						<h1 class="glyphicon glyphicon-pencil"> <?=$title;?></h1>
					</div>
					
					<div class="col-md-12">
						<div class="edit-task">
							<form class="add-task" action="" id="form-task">
								<div class="form-group">
									<label for="name_task">Название задачи</label>
									<input type="text" id="name_task" name="name_task" placeholder="Название задачи" class="form-control">
								</div>
								<div class="form-group">
									<label for="description_task">Описание задачи</label>
									<textarea class="form-control" name="description_task" id="description_task"></textarea>
								</div>
								<div class="form-group">
									<label for="preoritet-task">Приоритет задачи</label>
									<select name="preoritet-task" id="preoritet-task" class="form-control">
										<? foreach($prioritety as $item):?>
											<option value="<?=$item['id_preoritet_task']?>"><?=$item['name_preoritet_task']?></option>
										<? endforeach;?>
									</select>
								</div>
								<div class="form-group">
									<label for="deadline">Крайний срок</label>
									<input type="text" id="deadline" name="deadline" placeholder="01.01.2016" class="form-control">
								</div>
								<div class="form-group">
									<label for="perfomer">Исполнитель</label>
									<select name="perfomer" id="perfomer" class="form-control">
										<? foreach($personal as $person):?>
										<option value="<?=$person['id_personal'];?>"><?=$person['firstname'];?> <?=$person['name_personal'];?></option>
										<? endforeach;?>
									</select>
								</div>
								<div class="form-group">
									<label for="postanov">Постановщик</label>
									<select name="postanov" id="postanov" class="form-control">
										<? foreach($personal as $person):?>
										<option value="<?=$person['id_personal'];?>"><?=$person['firstname'];?> <?=$person['name_personal'];?></option>
										<? endforeach;?>
									</select>
								</div>
								<div class="form-group">
									<label for="project">Проект</label>
									<select name="project" id="project" class="form-control">
										<? foreach($project as $proj):?>
										<option value="<?=$proj['id_project'];?>"><?=$proj['name_project'];?></option>
										<? endforeach;?>
									</select>
								</div>
								<input type="hidden" name="id_task" value="<?=$_GET['id_task'];?>">
								<div class="form-group">
									<input type="submit" class="btn btn-success" name="btn_save" id="btn_save" value="Сохранить">
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>

		<div class="empty"></div>
		<footer></footer>

		<div class="modal fade" id="modal-warning">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Создание/изменение задачи</h4>
					</div>
					<div class="modal-body bg-danger">
						<p>Создание/изменение задачи выполнить не удалось. Попробуйте снова.</p>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<div class="modal fade" id="modal-success">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Создание/изменение задачи</h4>
					</div>
					<div class="modal-body bg-success">
						<p>Создание/изменение задачи выполнено успешно</p>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<script src="/tpl/js/jquery-3.1.1.min.js"></script>
		<script src="/tpl/js/bootstrap.min.js"></script>

		<script src="/tpl/js/main.js"></script>
		<script>
			$(document).ready(function(){
				$("#form-task").submit(function(){
					var dataForm = $(this).serialize();
					var form = $(this);

					$.ajax({
						type: 'POST',
						url: '/controller/task.php',
						data: dataForm,
						success: function(data){
							if(data[0] == true){
								$("#modal-success").modal();
								setInterval(function(){
									window.location.replace('/?page=task');
								}, 3000);
							}else{
								$("#modal-warning").modal();
							}
						}
					});
					return false;
				});
			});
		</script>
	</div>
</body>
</html>