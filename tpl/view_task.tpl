<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Описание задачи</title>
	<link rel="stylesheet" href="/tpl/css/bootstrap.min.css">
	<link rel="stylesheet" href="/tpl/css/normalize.css">
	<!--my css-->
	<link rel="stylesheet" href="/tpl/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--navigation-->
					<nav class="navbar navbar-default" role="navigation">
						<div class="container">
							<div class="navbar-header">
								<a href="/" class="navbar-brand">PMS</a>
								<p class="navbar-text">Система управления проектами</p>
							</div>
							<ul class="nav navbar-nav">
								<?php include "/tpl/inc/menu.php"; ?>
							</ul>
							<button type="button" id="enter_site" class="btn btn-default navbar-btn">Войти</button>
						</div>
					</nav>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--тут будут выводиться сообщения-->
				</div>
			</div>
		</div>

		<!--content-->
		<div class="container">
			<div class="row">
				<!--left-bar-->
				<div class="col-md-2">
					<div class="container">
						<div class="col-md-2">
							<div class="col-md-10">
								<img src="/tpl/images/avatar.png" alt="" class="img-responsive img-thumbnail">
							</div>
							<div class="name-user">
								<h4>Николай Ляхов</h4>
							</div>
						</div>
					</div>
					<ul class="nav nav-pils nav-stacked">
						<?php include "/tpl/inc/left_menu.php";?>
					</ul>
				</div>
				
				<!--right-bar-->
				<div class="col-md-10">
					<div class="page-header">
						<h1 class="glyphicon glyphicon-tag"> <?=$getTask['name_task'];?> #<?=$getTask['id_task'];?></h1>
					</div>
					
					<div class="col-md-12">
						<nav class="navbar">
							<ul class="nav navbar-nav">
								<li><a href="#"><span class="glyphicon glyphicon-arrow-left"></span> К списку</a></li>	
								<li><a href="#">Все</a></li>	
								<li><a href="#">Выполняю</a></li>	
								<li><a href="#">Поручил</a></li>	
								<li><a href="/?page=task&action=new_task">Новая задача</a></li>	
							</ul>
						</nav>
					</div>

					<div class="col-md-8">
						<div class="content-task">
							<div class="col-md-12">
								<div class="name-task"><p><?=$getTask['name_task'];?></p></div>
							</div>
							<div class="col-md-12">
								<p><?=$getTask['description_task'];?></p>
								<div class="project_task">
									Задача в проекте: <?=$getTask['name_project'];?>
								</div>
							</div>
						</div>
						<input type="hidden" id="id_task" value="<?=$getTask['id_task'];?>">
						<div class="col-md-12">

								<? if($getTask['id_status_task']==1):?>
							<div class="btn-group perfomer">
								<button id='btn-success' type='button' class='btn btn-success'>Начать выполнение</button>
							</div>
								<? endif;?>
							<? if($getTask['id_status_task']==5):?>
							<div class="btn-group perfomer">
								<button id='btn-success' type='button' class='btn btn-success'>Начать выполнение</button>
							</div>
							<? endif;?>

							<? if($getTask['id_status_task']==2):?>
								<div class="btn-group perfomer">
								<button id='btn-pause' type='button' class='btn btn-warning'>Приостановить</button>
								<button id='btn-proverka' type='button' class='btn btn-success'>Завершить</button>
								</div>
									<? endif;?>
								<? if($getTask['id_status_task']==3):?>
								<div class="btn-group director">
									<button id='btn-ok' type="button" class="btn btn-success">Принять</button>
									<button id='btn-back' type="button" class="btn btn-danger">Вернуть</button>
								</div>
								<? endif;?>





						</div>
					</div>
					<div class="col-md-4">
						<table class="table">
							<tr>
								<th>Крайний срок</th>
								<td><?=$getTask['deadline_task'];?></td>
							</tr>
							<tr>
								<th>Поставлена</th>
								<td><?=$getTask['date_postanovka'];?></td>
							</tr>
							<tr>
								<th>Старт</th>
								<td><?=$getTask['start_date'];?></td>
							</tr>
							<tr>
								<th>Завершена</th>
								<td><?=$getTask['finish_date'];?></td>
							</tr>
							<tr>
								<th>Ответственный</th>
								<td><?=$getTask['firstname'];?> <?=$getTask['name_personal'];?></td>
							</tr>
						</table>
					</div>
					
				</div>
			</div>
		</div>

		<div class="empty"></div>


		<footer></footer>
		<script src="/tpl/js/jquery-3.1.1.min.js"></script>
		<script src="/tpl/js/bootstrap.min.js"></script>

		<script src="/tpl/js/highcharts.js"></script>
		<script src="/tpl/js/exporting.js"></script>
		<script src="https://code.highcharts.com/highcharts-3d.js"></script>
		<script src="https://code.highcharts.com/modules/data.js"></script>
		<script src="https://code.highcharts.com/modules/drilldown.js"></script>

		<script src="/tpl/js/main.js"></script>
	</div>
</body>
</html>