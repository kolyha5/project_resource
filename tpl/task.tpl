<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Список задач</title>
	<link rel="stylesheet" href="/tpl/css/bootstrap.min.css">
	<link rel="stylesheet" href="/tpl/css/normalize.css">
	<!--my css-->
	<link rel="stylesheet" href="/tpl/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--navigation-->
					<nav class="navbar navbar-default" role="navigation">
						<div class="container">
							<div class="navbar-header">
								<a href="/" class="navbar-brand">PMS</a>
								<p class="navbar-text">Система управления проектами</p>
							</div>
							<ul class="nav navbar-nav">
								<?php include "/tpl/inc/menu.php"; ?>
							</ul>
							<button type="button" id="enter_site" class="btn btn-default navbar-btn">Войти</button>
						</div>
					</nav>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--тут будут выводиться сообщения-->
				</div>
			</div>
		</div>

		<!--content-->
		<div class="container">
			<div class="row">
				<!--left-bar-->
				<div class="col-md-2">
					<div class="container">
						<div class="col-md-2">
							<div class="col-md-10">
								<img src="/tpl/images/avatar.png" alt="" class="img-responsive img-thumbnail">
							</div>
							<div class="name-user">
								<h4>Николай Ляхов</h4>
							</div>
						</div>
					</div>
					<ul class="nav nav-pils nav-stacked">
						<?php include "/tpl/inc/left_menu.php";?>
					</ul>
				</div>
				
				<!--right-bar-->
				<div class="col-md-10">
					<div class="page-header">
						<h1 class="glyphicon glyphicon-list"> Список задач</h1>
					</div>
					
					<div class="col-md-12">
						<nav class="navbar">
							<ul class="nav navbar-nav">
								<li><a href="#">Все</a></li>	
								<li><a href="#">Выполняю</a></li>	
								<li><a href="#">Поручил</a></li>	
								<li><a href="/?page=task&action=new_task">Новая задача</a></li>	

							</ul>
						</nav>
					</div>

					<div class="col-md-12">
						<table class="table table-striped">
							<tr>
								<th>Название</th>
								<th>Статус</th>
								<th>Приоритет</th>
								<th>Крайний срок</th>
								<th>Постановщик</th>
							</tr>
							<? foreach($data_task as $task):?>
							<tr>
								<td><a href="/?page=task&action=view_task&id_task=<?=$task['id_task'];?>"><?=$task['name_task'];?></a></td>
								<td><?=$task['name_status_task'];?></td>
								<td><?=$task['name_preoritet_task'];?></td>
								<td><?=$task['deadline_task'];?></td>
								<td><?=$task['postanov'];?></td>
							</tr>
							<? endforeach;?>
						</table>
					</div>
					
				</div>
			</div>
		</div>

		<div class="empty"></div>


		<footer></footer>
		<script src="/tpl/js/jquery-3.1.1.min.js"></script>
		<script src="/tpl/js/bootstrap.min.js"></script>

		<script src="/tpl/js/highcharts.js"></script>
		<script src="/tpl/js/exporting.js"></script>
		<script src="https://code.highcharts.com/highcharts-3d.js"></script>
		<script src="https://code.highcharts.com/modules/data.js"></script>
		<script src="https://code.highcharts.com/modules/drilldown.js"></script>

		<script src="/tpl/js/main.js"></script>
	</div>
</body>
</html>