<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Просмотр профиля сотрудника</title>
	<link rel="stylesheet" href="/tpl/css/bootstrap.min.css">
	<link rel="stylesheet" href="/tpl/css/normalize.css">
	<!--my css-->
	<link rel="stylesheet" href="/tpl/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--navigation-->
					<nav class="navbar navbar-default" role="navigation">
						<div class="container">
							<div class="navbar-header">
								<a href="/" class="navbar-brand">PMS</a>
								<p class="navbar-text">Система управления проектами</p>
							</div>
							<ul class="nav navbar-nav">
								<?php include "/tpl/inc/menu.php"; ?>
							</ul>
							<button type="button" id="enter_site" class="btn btn-default navbar-btn">Войти</button>
						</div>
					</nav>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--тут будут выводиться сообщения-->
				</div>
			</div>
		</div>

		<!--content-->
		<div class="container">
			<div class="row">
				<!--left-bar-->
				<div class="col-md-2">
					<div class="container">
						<div class="col-md-2">
							<div class="col-md-10">
								<img src="/tpl/images/avatar.png" alt="" class="img-responsive img-thumbnail">
							</div>
							<div class="name-user">
								<h4>Николай Ляхов</h4>
								<small>Руководитель организации</small>
							</div>
						</div>
					</div>
					<ul class="nav nav-pils nav-stacked">
						<?php include "/tpl/inc/left_menu.php";?>
					</ul>
				</div>
				
				<!--right-bar-->
				<div class="col-md-10">
					<div class="page-header">
						<h1 class="glyphicon glyphicon-"> Николай Ляхов: просмотр профиля</h1>
					</div>
					
					<div class="col-md-12">
						<nav class="navbar">
							<ul class="nav navbar-nav">
								<li><a href="#"><span class="glyphicon glyphicon-arrow-left"></span> К списку</a></li>	
							</ul>
						</nav>
					</div>

					<div class="col-md-8">
						<div class="content-task">
							<div class="col-md-12">
								<table class="table">
									<tr>
										<th>Фамилия</th>
										<td>Ляхов</td>
									</tr>
									<tr>
										<th>Имя</th>
										<td>Николай</td>
									</tr>
									<tr>
										<th>Должность</th>
										<td>Руководитель организации</td>
									</tr>
									<tr>
										<th>Дата рождения</th>
										<td>22.06.1993</td>
									</tr>
									<tr>
										<th>E-mail</th>
										<td><a href="mailto:kolyha5@yandex.ru">kolyha5@yandex.ru</a></td>
									</tr>
									<tr>
										<th>Телефон</th>
										<td><a href="tel:+79506986639">+79506986639</a></td>
									</tr>
									<tr>
										<th title="Проекты, которыми управляет сотрудник">Список проектов</th>
										<td>
											<ul class="list">
												<li><a href="#">Проект 1</a></li>
												<li><a href="#">Проект 2</a></li>
												<li><a href="#">Проект 3</a></li>
											</ul>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="col-md-12">
							<a  href="/?page=personal&action=edit_personal">Редактировать</a>
						</div>
					</div>
					<div class="col-md-4">
						<table class="table">
							<tr>
								<th>Последний вход в систему</th>
								<td>10.11.2016</td>
							</tr>
						</table>
					</div>
					
				</div>
			</div>
		</div>

		<div class="empty"></div>


		<footer></footer>
		<script src="/tpl/js/jquery-3.1.1.min.js"></script>
		<script src="/tpl/js/bootstrap.min.js"></script>

		<script src="/tpl/js/highcharts.js"></script>
		<script src="/tpl/js/exporting.js"></script>
		<script src="https://code.highcharts.com/highcharts-3d.js"></script>
		<script src="https://code.highcharts.com/modules/data.js"></script>
		<script src="https://code.highcharts.com/modules/drilldown.js"></script>

		<script src="/tpl/js/main.js"></script>
	</div>
</body>
</html>