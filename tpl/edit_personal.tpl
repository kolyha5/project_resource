<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?=$title;?></title>
	<link rel="stylesheet" href="/tpl/css/bootstrap.min.css">
	<link rel="stylesheet" href="/tpl/css/normalize.css">
	<!--my css-->
	<link rel="stylesheet" href="/tpl/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--navigation-->
					<nav class="navbar navbar-default" role="navigation">
						<div class="container">
							<div class="navbar-header">
								<a href="/" class="navbar-brand">PMS</a>
								<p class="navbar-text">Система управления проектами</p>
							</div>
							<ul class="nav navbar-nav">
								<?php include "/tpl/inc/menu.php"; ?>
							</ul>
							<button type="button" id="enter_site" class="btn btn-default navbar-btn">Войти</button>
						</div>
					</nav>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!--тут будут выводиться сообщения-->
				</div>
			</div>
		</div>

		<!--content-->
		<div class="container">
			<div class="row">
				<!--left-bar-->
				<div class="col-md-2">
					<div class="container">
						<div class="col-md-2">
							<div class="col-md-10">
								<img src="/tpl/images/avatar.png" alt="" class="img-responsive img-thumbnail">
							</div>
							<div class="name-user">
								<h4>Николай Ляхов</h4>
								<small>Руководитель организации</small>
							</div>
						</div>
					</div>
					<ul class="nav nav-pils nav-stacked">
						<?php include "/tpl/inc/left_menu.php";?>
					</ul>
				</div>
				
				<!--right-bar-->
				<div class="col-md-10">
					<div class="page-header">
						<h1 class="glyphicon glyphicon-pencil"> <?=$title;?></h1>
					</div>
					
					<div class="col-md-12">
						<div class="edit-task">
							<form action="">
								<div class="form-group">
									<label for="first_name">Фамилия</label>
									<input type="text" id="first_name" name="first_name" placeholder="Например, Иванов" class="form-control">
								</div>
								<div class="form-group">
									<label for="name">Имя</label>
									<input type="text" id="name" name="name" placeholder="Например, Иван" class="form-control">
								</div>
								<div class="form-group">
									<label for="name_task">Дата рождения</label>
									<input type="text" id="name_task" name="name_task" placeholder="Например, 01.01.1990" class="form-control">
								</div>
								<div class="form-group">
									<label for="name_task">E-mail</label>
									<input type="text" id="name_task" name="name_task" placeholder="Например, info@domen.ru" class="form-control">
								</div>
								<div class="form-group">
									<label for="name_task">Телефон</label>
									<input type="text" id="name_task" name="name_task" placeholder="Например, +79001234567" class="form-control">
								</div>
								<div class="form-group">
									<label for="dolzhnost">Должность</label>
									<select name="dolzhnost" id="dolzhnost" class="form-control">
										<option value="Николай Ляхов">Руководитель организации</option>
										<option value="Татьяна Ляхова">Генеральный директор</option>
										<option value="Татьяна Ляхова">Менеджер</option>
									</select>
								</div>
								<div class="form-group">
									<input type="button" class="btn btn-success" name="btn_save" id="btn_save" value="Сохранить">
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>

		<div class="empty"></div>


		<footer></footer>
		<script src="/tpl/js/jquery-3.1.1.min.js"></script>
		<script src="/tpl/js/bootstrap.min.js"></script>

		<script type="text/javascript" src="/tpl/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="/tpl/AjexFileManager/ajex.js"></script>
		<script type="text/javascript">
			var ckeditor1 = CKEDITOR.replace( 'description_task' );
			AjexFileManager.init({
				returnTo: 'ckeditor',
				editor: ckeditor1
			});
		</script>

		<script src="/tpl/js/main.js"></script>
	</div>
</body>
</html>