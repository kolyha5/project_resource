$(document).ready(function(){
    $.ajax({
        url: "status_task.php",
        cache: false,
        success: function(data){
            //alert('1')
            statusTask(data.play , data.playOzhid , data.playProverka , data.close , data.pause);
        }
    });

//график Статусы задач

    function statusTask(play, playOzhid, playProverka, close, pause) {
        Highcharts.chart('container_pie', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: 'Статусы задач'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Количество',
                data: [
                    ['Выполняется', play],
                    ['Ожидает выполнения', playOzhid],
                    ['Ожидает проверки', playProverka],
                    ['Закрыта', close],
                    ['На паузе', pause]
                ]
            }]
        });
    }

    $(function () {

        Highcharts.chart('container', {
            title: {
                text: 'ЗП сотрудников за последний квартал 2016 года',
                x: -20 //center
            },
            xAxis: {
                categories: ['Октябрь', 'Ноябрь', 'Декабрь']
            },
            yAxis: {
                title: {
                    text: 'Зарплата (руб)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' руб'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Петров',
                data: [35432, 34499, 35954]
            },
                {
                    name: 'Иванов',
                    data: [50769, 45788, 49173]
                },
                {
                    name: 'Сидоров',
                    data: [33990, 33024, 34734]
                },
                {
                    name: 'Федоров',
                    data: [34072, 33909, 34057]
                }
            ]
        });
    });

    $.ajax({
        url: "personal_tasks.php",
        success: function(data){
            personalTasks(data);
            console.log(data);
        }
    });

    function personalTasks(data) {
        Highcharts.chart('container_column', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Занятость сотрудников'
            },
            subtitle: {
                text: 'Выполняют задачи или скоро будут выполнять'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Количество задач'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> задач<br/>'
            },

            series: [{
                name: 'Информация',
                colorByPoint: true,
                data: [
                    {
                        name: 'Петров',
                        y: parseInt(data.petrov),
                        drilldown: 'Петров'
                    }, {
                        name: 'Иванов',
                        y: parseInt(data.ivanov),
                        drilldown: 'Иванов'
                    }, {
                        name: 'Сидоров',
                        y: parseInt(data.sidorov),
                        drilldown: 'Сидоров'
                    }, {
                        name: 'Федоров',
                        y: parseInt(data.fedorov),
                        drilldown: 'Федоров'
                    }
                ]
            }],

        });
    }



    $.ajax({
        url: "kol_tasks_project.php",
        success: function(data){
            kolTasksProject(data.one, data.two, data.thrid);
        }
    });

    function kolTasksProject(one, two, thrid) {
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container_column3',
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
            },
            title: {
                text: 'Количество задач в проектах'
            },
            subtitle: {
                text: 'Количество задач в статусах "Ожидает выполнения" и "Выполняется"'
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            series: [{
                name: "Проект №1",
                data: [parseInt(one)]
            },{
                name: "Проект №2",
                data: [parseInt(two)]
            },{
                name: "Проект №3",
                data: [parseInt(thrid)]
            }
            ]
        });

        function showValues() {
            $('#alpha-value').html(chart.options.chart.options3d.alpha);
            $('#beta-value').html(chart.options.chart.options3d.beta);
            $('#depth-value').html(chart.options.chart.options3d.depth);
        }

        // Activate the sliders
        $('#sliders input').on('input change', function () {
            chart.options.chart.options3d[this.id] = this.value;
            showValues();
            chart.redraw(false);
        });

        showValues();
    }

    $("#btn-success").click(function(){
        var id = $("#id_task").val();
        var dataTask = {idTask: id, type:"success"};
        $.ajax({
            type: "POST",
            url: "update_task.php",
            data:dataTask,
            success: function(data){
                if(data.result){
                    console.log(dataTask);
                    alert("Вы успешно начали выполнять задачу");
                    window.location.reload();
                }
            }
        });
    });

    $("#btn-pause").click(function(){
        var id = $("#id_task").val();
        var dataTask = {idTask: id, type:"pause"};
        console.log(dataTask);
        $.ajax({
            type: "POST",
            url: "update_task.php",
            data:dataTask,
            success: function(data){
                if(data.result){
                    console.log(dataTask);
                    alert("Вы приостановили выполнение задачи");
                    window.location.reload();
                }
            }
        });
    });

    $("#btn-proverka").click(function(){
        var id = $("#id_task").val();
        var dataTask = {idTask: id, type:"proverka"};
        $.ajax({
            type: "POST",
            url: "update_task.php",
            data:dataTask,
            success: function(data){
                if(data.result){
                    console.log(dataTask);
                    alert("Вы отправили задачу на проверку");
                    window.location.reload();
                }
            }
        });
    });

    $("#btn-ok").click(function(){
        var id = $("#id_task").val();
        var dataTask = {idTask: id, type:"close"};
        $.ajax({
            type: "POST",
            url: "update_task.php",
            data:dataTask,
            success: function(data){
                if(data.result){
                    console.log(dataTask);
                    alert("Вы приняли задачу");
                    window.location.reload();
                }
            }
        });
    });

});
