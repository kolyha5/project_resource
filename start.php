<?php
//файл с настройками
session_start();

mb_internal_encoding("UTF-8");

ini_set("display_errors", 1);

set_include_path(get_include_path().PATH_SEPARATOR."core".PATH_SEPARATOR."controller");
spl_autoload_extensions("_class.php");
spl_autoload_register();

define("DIR_TPL", "/tpl/");
//define("MAIN_LAYOUT", "main");


