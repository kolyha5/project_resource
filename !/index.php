<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Система управления проектами</title>

        <link href="css/style.default.css" rel="stylesheet">
        <link href="css/morris.css" rel="stylesheet">
        <link href="css/select2.css" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        
        <header>
            <div class="headerwrapper">
                <div class="header-left">
                    <a href="index.html" class="logo">
                        <img src="images/logo.png" alt="" /> 
                    </a>
                    <div class="pull-right">
                        <a href="" class="menu-collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div><!-- header-left -->
                
                <div class="header-right">
                    
                    <div class="pull-right">
                        
                        <form class="form form-search" action="search-results.html">
                            <input type="search" class="form-control" placeholder="Поиск..." />
                        </form>
                        
                        <div class="btn-group btn-group-list btn-group-notification">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-bell-o"></i>
                              <span class="badge">1</span>
                            </button>
                            <div class="dropdown-menu pull-right">
                                <a href="" class="link-right"><i class="fa fa-search"></i></a>
                                <h5>Уведомления</h5>
                                <ul class="media-list dropdown-list">
                                    <li class="media">
                                        <img class="img-circle pull-left noti-thumb" src="images/photos/user1.png" alt="">
                                        <div class="media-body">
                                          <strong>Nusja Nawancali</strong> likes a photo of you
                                          <small class="date"><i class="fa fa-thumbs-up"></i> 15 минут назад</small>
                                        </div>
                                    </li>
                                   
                                </ul>
                                <div class="dropdown-footer text-center">
                                    <a href="" class="link">Показать все уведомления</a>
                                </div>
                            </div><!-- dropdown-menu -->
                        </div><!-- btn-group -->
                        
                        <div class="btn-group btn-group-list btn-group-messages">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge">1</span>
                            </button>
                            <div class="dropdown-menu pull-right">
                                <a href="" class="link-right"><i class="fa fa-plus"></i></a>
                                <h5>Новые сообщения</h5>
                                <ul class="media-list dropdown-list">
                                    <li class="media">
                                        <span class="badge badge-success">Новый</span>
                                        <img class="img-circle pull-left noti-thumb" src="images/photos/user1.png" alt="">
                                        <div class="media-body">
                                          <strong>Nusja Nawancali</strong>
                                          <p>Hi! How are you?...</p>
                                          <small class="date"><i class="fa fa-clock-o"></i> 15 минут назад</small>
                                        </div>
                                    </li>
                                    
                                </ul>
                                <div class="dropdown-footer text-center">
                                    <a href="" class="link">Показать все сообщения</a>
                                </div>
                            </div><!-- dropdown-menu -->
                        </div><!-- btn-group -->
                        
                        <div class="btn-group btn-group-option">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                              <li><a href="#"><i class="glyphicon glyphicon-user"></i>Мой профиль</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-star"></i> Activity Log</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-cog"></i>Настройки</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i>Помощь</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="glyphicon glyphicon-log-out"></i>Выход из системы</a></li>
                            </ul>
                        </div><!-- btn-group -->
                        
                    </div><!-- pull-right -->
                    
                </div><!-- header-right -->
                
            </div><!-- headerwrapper -->
        </header>
        
        <section>
            <div class="mainwrapper">
                <div class="leftpanel">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="profile.html">
                            <img class="img-circle" src="images/photos/profile.png" alt="">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">Николай Ляхов</h4>
                            <small class="text-muted">Руководитель организации</small>
                        </div>
                    </div><!-- media -->
                    
                    <h5 class="leftpanel-title">Навигация</h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a href="index.html"><i class="fa fa-home"></i> <span>Панель управления</span></a></li>
                        <li><a href="messages.html"><span class="pull-right badge">1</span><i class="fa fa-envelope-o"></i> <span>Сообщения</span></a></li>
                    </ul>
                    
                </div><!-- leftpanel -->
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                                    <li>Панель управления</li>
                                </ul>
                                <h4>Панель управления</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">
                        
                        <div class="row row-stat">
                            <div class="col-md-4">
                                <div class="panel panel-success-alt noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-dollar"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Доход сегодня</h5>
                                            <h1 class="mt5">8 102,32 руб</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Вчера</h5>
                                                <h4 class="nomargin">29 009,17 руб</h4>
                                            </div>
                                            <div class="pull-right">
                                                <h5 class="md-title nomargin">На этой неделе</h5>
                                                <h4 class="nomargin">99 103,67 руб</h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-4 -->
                            
                            <div class="col-md-4">
                                <div class="panel panel-primary noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-users"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Новые пользователи</h5>
                                            <h1 class="mt5">138 102</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Вчера</h5>
                                                <h4 class="nomargin">10 009</h4>
                                            </div>
                                            <div class="pull-right">
                                                <h5 class="md-title nomargin">На этой неделе</h5>
                                                <h4 class="nomargin">178 222</h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-4 -->
                            
                            <div class="col-md-4">
                                <div class="panel panel-dark noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" data-placement="left" title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-pencil"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Новые записи</h5>
                                            <h1 class="mt5">153 900</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Вчера</h5>
                                                <h4 class="nomargin">144 009</h4>
                                            </div>
                                            <div class="pull-right">
                                                <h5 class="md-title nomargin">На этой неделе</h5>
                                                <h4 class="nomargin">987 212</h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-4 -->
                        </div><!-- row -->
                        
                        <div class="alert alert-info">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                            <strong>Внимание!</strong> Это <a class="alert-link" href="">сообщение нуждается в вашем внимании</a>, но оно не очень важно.
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-body padding15">
                                        <h5 class="md-title mt0 mb10">Статистика сайта</h5>
                                        <div id="basicFlotLegend" class="flotLegend"></div>
                                        <!--div id="basicflot" class="flotChart"></div-->
                                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                        <div class="tinystat pull-left">
                                            <div id="sparkline" class="chart mt5"></div>
                                            <div class="datainfo">
                                                <span class="text-muted">Среднее</span>
                                                <h4>9 201 руб.</h4>
                                            </div>
                                        </div><!-- tinystat -->
                                        <div class="tinystat pull-right">
                                            <div id="sparkline2" class="chart mt5"></div>
                                            <div class="datainfo">
                                                <span class="text-muted">Итог</span>
                                                <h4>67 201 руб.</h4>
                                            </div>
                                        </div><!-- tinystat -->
                                    </div><!-- panel-footer -->
                                </div><!-- panel -->
                            </div>
                            
                            
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-body padding15">
                                        <h5 class="md-title mt0 mb10">11</h5>
                                        <div id="basicFlotLegend" class="flotLegend"></div>
                                        <!--div id="basicflot" class="flotChart"></div-->
                                        <div id="container_column" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                        <div class="tinystat pull-left">
                                            <div id="sparkline" class="chart mt5"></div>
                                            <div class="datainfo">
                                                <span class="text-muted">Среднее</span>
                                                <h4>9 201 руб.</h4>
                                            </div>
                                        </div><!-- tinystat -->
                                        <div class="tinystat pull-right">
                                            <div id="sparkline2" class="chart mt5"></div>
                                            <div class="datainfo">
                                                <span class="text-muted">Итог</span>
                                                <h4>67 201 руб.</h4>
                                            </div>
                                        </div><!-- tinystat -->
                                    </div><!-- panel-footer -->
                                </div><!-- panel -->
                            </div>

                        </div><!-- row -->


                         <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-body padding15">
                                        <h5 class="md-title mt0 mb10">Статистика сайта</h5>
                                        <div id="basicFlotLegend" class="flotLegend"></div>
                                        <!--div id="basicflot" class="flotChart"></div-->
                                        <div id="container_pie" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                        <div class="tinystat pull-left">
                                            <div id="sparkline" class="chart mt5"></div>
                                            <div class="datainfo">
                                                <span class="text-muted">Среднее</span>
                                                <h4>9 201 руб.</h4>
                                            </div>
                                        </div><!-- tinystat -->
                                        <div class="tinystat pull-right">
                                            <div id="sparkline2" class="chart mt5"></div>
                                            <div class="datainfo">
                                                <span class="text-muted">Итог</span>
                                                <h4>67 201 руб.</h4>
                                            </div>
                                        </div><!-- tinystat -->
                                    </div><!-- panel-footer -->
                                </div><!-- panel -->
                            </div>
                            
                            
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-body padding15">
                                        <h5 class="md-title mt0 mb10">11</h5>
                                        <div id="basicFlotLegend" class="flotLegend"></div>
                                        <!--div id="basicflot" class="flotChart"></div-->
                                        <div id="container_column3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                        <div class="tinystat pull-left">
                                            <div id="sparkline" class="chart mt5"></div>
                                            <div class="datainfo">
                                                <span class="text-muted">Среднее</span>
                                                <h4>9 201 руб.</h4>
                                            </div>
                                        </div><!-- tinystat -->
                                        <div class="tinystat pull-right">
                                            <div id="sparkline2" class="chart mt5"></div>
                                            <div class="datainfo">
                                                <span class="text-muted">Итог</span>
                                                <h4>67 201 руб.</h4>
                                            </div>
                                        </div><!-- tinystat -->
                                    </div><!-- panel-footer -->
                                </div><!-- panel -->
                            </div>

                        </div><!-- row -->
                        
                        
                        
  
                        </div><!-- row -->
                        
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>


        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/retina.min.js"></script>
        <script src="js/jquery.cookies.js"></script>
        
        <script src="js/flot/jquery.flot.min.js"></script>
        <script src="js/flot/jquery.flot.resize.min.js"></script>
        <script src="js/flot/jquery.flot.spline.min.js"></script>
        <script src="js/jquery.sparkline.min.js"></script>
        <script src="js/morris.min.js"></script>
        <script src="js/raphael-2.1.0.min.js"></script>
        <script src="js/bootstrap-wizard.min.js"></script>
        <script src="js/select2.min.js"></script>
        

        <script src="highcharts.js"></script>
        <script src="exporting.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/dashboard.js"></script>
        <script src="https://code.highcharts.com/highcharts-3d.js"></script>
        <script src="https://code.highcharts.com/modules/data.js"></script>
        <script src="https://code.highcharts.com/modules/drilldown.js"></script>
        <script src="jquery-3.1.1.min.js"></script>
        <script src="main.js"></script>

    </body>
</html>
