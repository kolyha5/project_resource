<?php
header("Content-Type: application/json");
$mysql = @new mysqli("localhost", "root", "", "my_project");
$mysql->query("set names utf8");

$sql = "SELECT id_project as project, COUNT(id_status_task) as count
        FROM tasks
        WHERE id_status_task = 1 OR id_status_task = 2
        GROUP BY project
";
$result = $mysql->query($sql);
while($row = $result->fetch_all(MYSQL_ASSOC)){
    $data[] = $row;
}

$projectOne = 0;
$projectTwo = 0;
$projectThrid = 0;
foreach($data[0] as $item){
    if($item['project']=="1"){
        $projectOne = $item['count'];
    }elseif($item['project']=="2"){
        $projectTwo = $item['count'];
    }elseif($item['project']=="3"){
        $projectThrid = $item['count'];
    }
}

$data = [
    "one" => $projectOne,
    "two" => $projectTwo,
    "thrid" => $projectThrid
];

echo json_encode($data);