<?php
header("Content-Type: application/json");
$mysql = @new mysqli("localhost", "root", "", "my_project");
$mysql->query("set names 'utf8'");

$sql = "SELECT id_status_task as status
        FROM tasks
";
$result = $mysql->query($sql);
while($row = $result->fetch_all(MYSQL_ASSOC)){
    $data[] = $row;
}

//echo "<pre>";
//print_r($data[0]);
//echo "</pre>";
$dataPlay = 0;
$dataPlayOzhid = 0;
$dataOzhidProverka = 0;
$dataClose = 0;
$dataPause = 0;
foreach($data[0] as $item){
    if($item['status']==1){
        $dataPlayOzhid += 1;
    }elseif($item['status']==2){
        $dataPlay += 1;
    }elseif($item['status']==3){
        $dataOzhidProverka += 1;
    }elseif($item['status']==4){
        $dataClose += 1;
    }elseif($item['status']==5){
        $dataPause += 1;
    }
}

$data = [
    "play" => $dataPlay,
    "playOzhid" => $dataPlayOzhid,
    "playProverka" => $dataOzhidProverka,
    "close" => $dataClose,
    "pause" => $dataPause
];

//print_r($data);

echo json_encode($data);