<?php
/**
 * абстрактный контроллер, его наследуем остальными котнроллерами.
 * Здесь же подключаем шаблонизатор
 */
abstract class AbstractController{
	//сохраним объекты шаблонизатора
	protected $view;
	public $mysql;

	public function __construct($view){
		$this->view = $view;

		
	}

	abstract protected function render($file);

	protected function mysql_connect(){
		$host = "localhost";
		$user = "root";
		$pass = "";
		$db = "my_project";

		$mysql = @new mysqli($host, $user, $pass, $db);
		if (mysqli_connect_errno()) {
			die(mysqli_connect_error());
		}
		$sql = "set names 'utf8'";
		$result = $mysql->query($sql);
		if (!$result) {
			die($mysql->error);
		}

		return $mysql;
	}

	protected function debug($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}

}