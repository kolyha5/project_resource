<?php
class Route{
	public static function start(){
		if (isset($_GET['page'])) {
			$controller_name = $_GET['page'];
		}else{
			$controller_name = "main";
		}
		
		if (isset($_GET['action'])) {
			$action_name = $_GET['action'];
		}else{
			$action_name = "index";
		}

		$controller_name = $controller_name."controller";
		$action_name = "action".$action_name;

		$controller = new $controller_name;

		if (method_exists($controller, $action_name)) {
			$controller->$action_name();
		}else{
			echo "Данная страница не найдена";
			exit();
		}
		
	}
}