<?php
/**
 * шаблонизатор
 */
class View{
	//путь к директории с файлами-шаблонами
	private $dir_tmpl;

	public function __construct($dir_tmpl){
		$this->dir_tmpl = $dir_tmpl;
	}

	/**
	 * [render - подключение шаблонных файлов]
	 * @param  [type]  $file   [файл, который будем подключать]
	 * @param  [type]  $params [параметры, ассоциативный массив]
	 * @param  boolean $return [вернуть значение или вывести на экран]
	 */
	public function render($file, $params, $return = false){
		$template = $this->dir_tmpl.$file.".tpl";
		extract($params);
		ob_start();
		include($template);
		if ($return) return ob_get_clean();
		else echo ob_get_clean();
	}

}