<?php
header("Content-Type: application/json");
$mysql = @new mysqli("localhost", "root", "", "my_project");
$mysql->query("set names utf8");

$sql = "SELECT firstname, COUNT(id_status_task) AS count
        FROM tasks, personals
        WHERE tasks.id_ispolnitel = personals.id_personal AND (id_status_task = 1 OR id_status_task = 2)
        GROUP BY firstname
";
$result = $mysql->query($sql);
while($row = $result->fetch_all(MYSQL_ASSOC)){
    $data[] = $row;
}

$ivanov = 0;
$petrov = 0;
$sidorov = 0;
$fedorov = 0;
foreach($data[0] as $item){
    if($item['firstname'] == "Иванов"){
        $ivanov = $item['count'];
    }elseif($item['firstname'] == "Петров"){
        $petrov = $item['count'];
    }elseif($item['firstname'] == "Сидоров"){
        $sidorov = $item['count'];
    }elseif($item['firstname'] == "Федоров"){
        $fedorov = $item['count'];
    }
}

$data = [
    "ivanov" => $ivanov,
    "petrov" => $petrov,
    "sidorov" => $sidorov,
    "fedorov" => $fedorov
];

echo json_encode($data);